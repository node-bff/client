import React from "react";
import {
    Typography,
  } from '@material-ui/core';
import styled from 'styled-components';

const Wrapped = styled.div`
    flex: 1;
`;
const Title = styled(Typography)`
    &&& {
        padding-top: 20px;
        padding-bottom: 20px;
    font-size: 24px;
    line-height: 24px;
    }
`;
const Item = styled.div``
function PreviewData({data}: {data: any}) {
    if(data){
        const {getProduct} = data;
        const typeOfCall = getProduct.gender === 'MALE' ? 'mr' : 'ms';
        return (
            <Wrapped>
                <Title>Preview</Title>
                <Item>
                    <Typography>Thank {typeOfCall} {getProduct.name}, with the insurance package of your choice, you need to pay {getProduct.premium} for {getProduct.paymentTerm} </Typography>
                </Item>
            </Wrapped>
        );
    }
    else{
        return(<></>);
    }
  
}

export default PreviewData;
