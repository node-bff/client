import React, { useState } from "react";
import { GET_PRODUCT } from "../Graphql/Mutation";
import { useMutation } from "@apollo/client";
import styled, {withTheme} from 'styled-components';
import PreviewData from './PreviewData';
import {
    TextField as MuiTextField,
    Typography,
    MenuItem,
    Select as MuiSelect,
    InputLabel,
    Button as MuiButton,
  } from '@material-ui/core';
const Wrapped = styled.div`
  display: flex;
`;
const Form = styled.div`
  &&&{
    padding-left: 50px;
    flex: 1;
  }
`;
const FormItem = styled.div`
  &&&{
    padding-top: 10px;
  }
`;
const Title = styled(Typography)`
  &&& {
      padding-top: 20px;
      padding-bottom: 20px;
    font-size: 24px;
    line-height: 24px;
  }
`;
const TextField = styled(MuiTextField)`
    &&& {
        input {
            padding: 10px;
        }
    }
`;
const Select = withTheme(styled(MuiSelect)`
    &&& {
        min-width: 150px;

    }
`);
const Button = styled(MuiButton)`
    &&&{
        margin-right: 10px;
    }
`;
function FormInput() {
    const [name, setName] = useState("");
    const [gender, setGender] = useState("");
    const [dateOfBirth, setDateOfBirth] = useState("");
    const [paymentTerm, setPaymentTerm] = useState("");
    const [planCode, setPlanCode] = useState("");
    const [premium, setPremium] = useState("");
    const [getProduct, { error, data }] = useMutation(GET_PRODUCT);
    const onChangeGender = (event: React.ChangeEvent<{ value: string }>) => {
        setGender(event.target.value);
    };
    const onChangePlanCode = (event: React.ChangeEvent<{ value: string }>) => {
        setPlanCode(event.target.value);
    };
    const onChangePaymentTerm = (event: React.ChangeEvent<{ value: string }>) => {
        setPaymentTerm(event.target.value);
    };
    const showData = () =>{
        if(name === '' || gender === '' 
        || dateOfBirth === '' || paymentTerm === ''
        || planCode === '' || premium === '')
            return;
        getProduct({
            variables: {
                name,
                gender,
                dateOfBirth,
                paymentTerm,
                planCode,
                premium
            }
        })
    }
    const resetData = () =>{
        setName('');
        setGender('');
        setDateOfBirth('');
        setPaymentTerm('');
        setPlanCode('');
        setPremium('');
    }
    return(
        <Wrapped>
            <Form>
                <Title>Input Data</Title>
                <FormItem>
                    <InputLabel>Name</InputLabel>
                    <TextField
                        variant="outlined"
                        value={name}
                        onChange={(event) => {
                            setName(event.target.value);
                        }}
                        placeholder='Name'
                    />
                </FormItem>
                <FormItem>
                    <InputLabel>Gender</InputLabel>
                    <Select
                        labelId="demo-controlled-open-select-label"
                        id="demo-controlled-open-select"
                        value={gender}
                        onChange={onChangeGender}
                        >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="MALE">MALE</MenuItem>
                        <MenuItem value="FEMALE">FEMALE</MenuItem>
                    </Select>
                </FormItem>
                <FormItem>
                    <InputLabel>DOB</InputLabel>
                    <TextField
                        variant="outlined"
                        type="date"
                        value={dateOfBirth}
                        onChange={(event) => {
                            setDateOfBirth(event.target.value);
                        }}
                        placeholder='DOB'
                    />
                </FormItem>
                <FormItem>
                    <InputLabel>Plan</InputLabel>
                    <Select
                        labelId="demo-controlled-open-select-label"
                        id="demo-controlled-open-select"
                        value={planCode}
                        onChange={onChangePlanCode}
                        >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="T11A20">package 1 (benefit 200k)</MenuItem>
                        <MenuItem value="T11A50">package 2 (benefit 500k)</MenuItem>
                        <MenuItem value="T11AM1">package 3 (benefit 1M)</MenuItem>
                    </Select>
                </FormItem>
                <FormItem>
                    <InputLabel>Premium</InputLabel>
                    <TextField
                        variant="outlined"
                        value={premium}
                        onChange={(event) => {
                            setPremium(event.target.value);
                        }}
                        placeholder='Premium'
                    />
                </FormItem>
                <FormItem>
                    <InputLabel>Payment Term</InputLabel>
                    <Select
                        labelId="demo-controlled-open-select-label"
                        id="demo-controlled-open-select"
                        value={paymentTerm}
                        onChange={onChangePaymentTerm}
                        >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="YEARLY">YEARLY</MenuItem>
                        <MenuItem value="HALFYEARLY">HALFYEARLY</MenuItem>
                        <MenuItem value="QUARTERLY">QUARTERLY</MenuItem>
                        <MenuItem value="MONTHLY">MONTHLY</MenuItem>
                    </Select>
                </FormItem>
                <FormItem>
                    <Button 
                        variant="contained" 
                        color="primary" onClick={showData}>
                            Show Preview
                    </Button>
                    <Button 
                        variant="contained" 
                        color="secondary"
                        onClick={resetData}>
                            Reset
                    </Button>
                </FormItem>
            </Form>
            <PreviewData data={data} />
        </Wrapped>
    )
}

export default FormInput;